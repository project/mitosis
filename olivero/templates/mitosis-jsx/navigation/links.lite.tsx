import { Show, For } from '@builder.io/mitosis';

interface Props {
  heading?: {
    text: Drupal.Markup;
    level?: string;
    attributes?: Drupal.Attributes;
  }
  attributes?: Drupal.Attributes;
  links: {
    [index: string]: {
      link?: Drupal.Markup;
      text?: Drupal.Markup;
      attributes?: Drupal.Attributes;
      textAttributes?: Drupal.Attributes;
    }
  }
}

export default function Links(props: Props) {
  return (
    <>
      <Show when={ props.links }>
        <Show when={ props.heading }>
          {/* TODO: Figure out how to replace "h2" with a dynamic tag name (props.heading.level) in Mitosis.  */}
          <h2 { ...props.heading.attributes }>{ props.heading.text }</h2>
        </Show>
        <ul { ...props.attributes }>
          {/* TODO: "each" expects an array, but props.links is an object.  */}
          <For each={ props.links }>{ (item, index) =>
            <li { ...item.attributes } key={ index }>
              <Show when={ item.link }>
                { item.link }
              </Show>
              <Show when={ !item.link && item.textAttributes }>
                <span { ...item.textAttributes }>{ item.text }</span>
              </Show>
              <Show when={ !item.link && !item.textAttributes }>
                { item.text }
              </Show>
            </li>
          }</For>
        </ul>
      </Show>
    </>
  );
}
